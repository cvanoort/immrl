# keras_plastic

Experiments with [Differentiable Plasticity]((https://arxiv.org/abs/1804.02464)) in [Keras](https://keras.io/).

## Contents:

- `afpo_tests.sh`: Executes a large test using Age Fitness Pareto Optimization
    ([AFPO](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.375.6168&rep=rep1&type=pdf)) to evolve the weights
    of Keras models applied to [OpenAI `gym`](https://gym.openai.com/) tasks.
- `afpo_train.py`: Optimizes the weights of a Keras model applied to an [OpenAI `gym`](https://gym.openai.com/) environment using AFPO.
- `impala_evaluate.py`: Evaluates a policies created by `impala_train.py`.
- `impala_train.py`: Optimizes a policy to solve OpenAI `gym` environments using the [IMPALA algorithm](https://arxiv.org/abs/1802.01561).
- `plastic_layers.py`: Recurrent Keras layers augmented with [differentiable plasticity](https://arxiv.org/abs/1804.02464).
- `summarize_test.py`: Creates visualizations and statistics that summarize the results of experiments run by afpo_tests.sh.

## Getting Started:
```bash
git clone https://gitlab.com/omega1563/keras_plastic.git
cd keras_plastic
pip install -r requirements.txt
```

Notes:
 - `afpo_tests.sh` uses [GNU parallel](https://www.gnu.org/software/parallel/) to improve test runtime.
