import seaborn as sns
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
from itertools import product
import numpy as np
from scipy.stats import mannwhitneyu


def load_test_rewards(result_path):
    labels, rewards = [], []
    for p in sorted(result_path.glob('*/mean_eval_rewards.csv')):
        labels.append(p.parent.stem)
        rewards.append(pd.read_csv(p, header=None, squeeze=True))

    test_rewards = pd.concat(rewards, axis=1)
    test_rewards.columns = labels

    return test_rewards


def load_reward_paths(result_path, kind='mean'):
    labels, rewards = [], []
    for p in sorted(result_path.glob(f'*/{kind}_reward_paths.csv')):
        labels.append(p.parent.stem)
        df = pd.read_csv(p, header=None).stack().reset_index()
        df.columns = ['index', 'time', 'reward']
        df = df.drop('index', axis=1)
        df['model'] = [p.parent.stem for _ in range(len(df))]
        rewards.append(df)

    return pd.concat(rewards)


def main():
    result_path = Path('../out')
    sns.set()

    mean_reward_paths = load_reward_paths(result_path, kind='mean')
    ax = sns.lineplot(
        x='time',
        y='reward',
        hue='model',
        data=mean_reward_paths,
        ci=95,
        err_style='band',
        palette=sns.husl_palette(n_colors=len(mean_reward_paths.model.unique()), l=0.55),
        linewidth=1,
    )
    plt.ylabel('mean population reward')
    plt.xlabel('generation')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tight_layout()
    plt.savefig('../out/mean_reward_paths.png')

    fig, ax = plt.subplots()
    max_reward_paths = load_reward_paths(result_path, kind='max')
    ax = sns.lineplot(
        x='time',
        y='reward',
        hue='model',
        data=max_reward_paths,
        ci=95,
        err_style='band',
        palette=sns.husl_palette(n_colors=len(max_reward_paths.model.unique()), l=0.55),
        linewidth=1,
    )
    plt.ylabel('max population reward')
    plt.xlabel('generation')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.tight_layout()
    plt.savefig('../out/max_reward_paths.png')

    test_rewards = load_test_rewards(result_path)
    p_vals = np.zeros((len(test_rewards.columns), len(test_rewards.columns)))
    cols = sorted(test_rewards.columns)
    for (i, c1), (j, c2) in product(enumerate(cols), enumerate(cols)):
        u_stat, p_val = mannwhitneyu(test_rewards[c1].values, test_rewards[c2].values, alternative='greater')
        p_vals[i, j] = p_val

    fig, ax = plt.subplots()
    plt.title('Raw P-Values')
    plt.imshow(p_vals)
    locs, labels = plt.xticks()
    plt.xticks(locs, [''] + cols, rotation=90)
    plt.yticks(locs, [''] + cols)
    plt.grid(False)
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('../out/p_values.png')

    p_vals_discrete = p_vals.copy() < 0.05
    fig, ax = plt.subplots()
    plt.title('P-Values < 0.05')
    plt.imshow(p_vals_discrete)
    locs, labels = plt.xticks()
    plt.xticks(locs, [''] + cols, rotation=90)
    plt.yticks(locs, [''] + cols)
    plt.grid(False)
    plt.tight_layout()
    plt.savefig('../out/p_values_discrete.png')

    plt.close('all')


if __name__ == '__main__':
    main()
