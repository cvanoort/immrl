import ray
import ray.rllib.agents.impala as impala
from ray import tune
from ray.tune.registry import register_env
import argparse

from gym_vail.envs.maze import Maze


def get_parser():
    parser = argparse.ArgumentParser(
        description="Trains an agent on an OpenAI Gym task using the IMPALA algorithm.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-u',
        '--updates',
        type=int,
        default=100,
        help='Number of training updates applied to the network.',
    )
    parser.add_argument(
        '-c',
        '--cpu_count',
        type=int,
        default=1,
        help='Number of CPU cores available to the program.',
    )
    parser.add_argument(
        '-g',
        '--gpu_count',
        type=int,
        default=0,
        help='Number of GPUs available to the program.',
    )
    parser.add_argument(
        '-e',
        '--env_name',
        type=str,
        default='Maze-v0',
        help='OpenAI Gym environment to use as the learning task.',
    )

    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )

    return parser


def env_creator(env_config):
    return Maze()


args = vars(get_parser().parse_args())

register_env('Maze-v0', env_creator)
ray.init()
config = impala.DEFAULT_CONFIG.copy()
config["num_gpus"] = args['gpu_count']
config["num_workers"] = args['cpu_count']
config["env"] = args['env_name']


tune.run(
    'IMPALA',
    config=config,
    stop={'training_iteration': args['updates']},
    name='maze-test',
    checkpoint_freq=10,
    checkpoint_at_end=True,
)
