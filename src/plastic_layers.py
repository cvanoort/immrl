"""
TODO: Add docstrings.
TODO: Validate that the plasticity is applied in the correct locations.
TODO: The new plasticity implementation as RNN states is much cleaner looking,
    but performance has suffered compared to the naive weight implementation.
    Can we get the best of both worlds?
"""

from keras import backend as K
from keras.layers import SimpleRNNCell, SimpleRNN, GRUCell, GRU, LSTMCell, LSTM
from keras.layers.recurrent import _generate_dropout_mask


def hebb(lr, prev_output, curr_output, plastic_vals):
    """
    Implements a classic Hebbian plasticity update rule.

    Currently only tested where prev_output/curr_output have shape (batch_size, time_steps, features).

    Args:
        lr: float or keras.Tensor, Learning rate / step size for the plasticity update.
        prev_output: keras.Tensor, Pre-synaptic activations.
        curr_output: keras.Tensor, Post-synaptic activations.
        plastic_vals: keras.Tensor, Current plasticity values.

    Returns: keras.Tensor
        Updated plasticity values.
    """
    return lr * K.dot(K.expand_dims(prev_output, 2), K.expand_dims(curr_output, 1)) + (1 - lr) * plastic_vals


def oja(lr, prev_output, curr_output, plastic_vals):
    """
    Implements Oja's plasticity update rule.

    Currently only tested where prev_output/curr_output have shape (batch_size, time_steps, features).

    Args:
        lr: float or keras.Tensor, Learning rate / step size for the plasticity update.
        prev_output: keras.Tensor, Pre-synaptic activations.
        curr_output: keras.Tensor, Post-synaptic activations.
        plastic_vals: keras.Tensor, Current plasticity values.

    Returns: keras.Tensor
        Updated plasticity values.
    """
    return plastic_vals + lr * K.expand_dims(curr_output, 1) * (
            K.expand_dims(prev_output, 2) - plastic_vals * K.expand_dims(curr_output, 1))


class PlasticRNNCell(SimpleRNNCell):
    """
    Extends the Keras SimpleRNNCell with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            plasticity_rule='oja',
            **kwargs,
    ):
        super().__init__(units, **kwargs)
        self.state_size = (self.units, self.units**2)

        self.plastic_kernel = None
        self.plastic_lr = None
        self.plasticity_rule = plasticity_rule
        self.plastic_update = oja if plasticity_rule == 'oja' else hebb

    def build(self, input_shape):
        self.plastic_kernel = self.add_weight(
            shape=(1, self.units, self.units),
            name='plastic_kernel',
            initializer=self.kernel_initializer,
            regularizer=self.kernel_regularizer,
            constraint=self.kernel_constraint,
        )
        self.plastic_lr = self.add_weight(
            shape=(1,),
            name='plastic_lr',
            initializer=self.bias_initializer,
            regularizer=self.bias_regularizer,
            constraint=self.bias_constraint,
        )

        super().build(input_shape)

    def call(self, inputs, states, training=None):
        prev_output = states[0]
        plastic_vals = K.reshape(states[1], (-1, self.units, self.units))

        if 0 < self.dropout < 1 and self._dropout_mask is None:
            self._dropout_mask = _generate_dropout_mask(
                K.ones_like(inputs),
                self.dropout,
                training=training,
            )
        if (0 < self.recurrent_dropout < 1 and
                self._recurrent_dropout_mask is None):
            self._recurrent_dropout_mask = _generate_dropout_mask(
                K.ones_like(prev_output),
                self.recurrent_dropout,
                training=training,
            )

        dp_mask = self._dropout_mask
        rec_dp_mask = self._recurrent_dropout_mask

        if dp_mask is not None:
            h = K.dot(inputs * dp_mask, self.kernel)
        else:
            h = K.dot(inputs, self.kernel)

        if self.bias is not None:
            h = K.bias_add(h, self.bias)

        if rec_dp_mask is not None:
            prev_output *= rec_dp_mask

        output = h + K.dot(prev_output, self.recurrent_kernel)

        # Apply plasticity
        output = output + K.batch_dot(prev_output, self.plastic_kernel * plastic_vals)

        if self.activation is not None:
            output = self.activation(output)

        plastic_vals = self.plastic_update(self.plastic_lr, prev_output, output, plastic_vals)

        # Properly set learning phase on output tensor.
        if 0 < self.dropout + self.recurrent_dropout:
            if training is None:
                output._uses_learning_phase = True
        return output, [output, K.batch_flatten(plastic_vals)]

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        return dict(**base_config, **config)


class PlasticRNN(SimpleRNN):
    """
    Augments the Keras SimpleRNN with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            activation='tanh',
            bias_constraint=None,
            bias_initializer='zeros',
            bias_regularizer=None,
            dropout=0.,
            kernel_constraint=None,
            kernel_initializer='glorot_uniform',
            kernel_regularizer=None,
            plasticity_rule='oja',
            recurrent_constraint=None,
            recurrent_dropout=0.,
            recurrent_initializer='orthogonal',
            recurrent_regularizer=None,
            use_bias=True,
            return_sequences=False,
            return_state=False,
            go_backwards=False,
            stateful=False,
            unroll=False,
            **kwargs,
    ):
        super().__init__(
            units,
            go_backwards=go_backwards,
            return_sequences=return_sequences,
            return_state=return_state,
            stateful=stateful,
            unroll=unroll,
            **kwargs,
        )
        self.plasticity_rule = plasticity_rule

        self.cell = PlasticRNNCell(
            units,
            activation=activation,
            bias_constraint=bias_constraint,
            bias_initializer=bias_initializer,
            bias_regularizer=bias_regularizer,
            dropout=dropout,
            kernel_constraint=kernel_constraint,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            plasticity_rule=plasticity_rule,
            recurrent_constraint=recurrent_constraint,
            recurrent_dropout=recurrent_dropout,
            recurrent_initializer=recurrent_initializer,
            recurrent_regularizer=recurrent_regularizer,
            use_bias=use_bias,
        )

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        del base_config['cell']
        return dict(**base_config, **config)


class PlasticGRUCell(GRUCell):
    """
    Augments the Keras GRUCell with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            plasticity_rule='oja',
            **kwargs
    ):
        super().__init__(units, **kwargs)
        self.state_size = (self.units, self.units ** 2)

        self.plastic_kernel = None
        self.plastic_lr = None
        self.plasticity_rule = plasticity_rule
        self.plastic_update = oja if plasticity_rule == 'oja' else hebb
        self.plastic_val = None

    def build(self, input_shape):
        self.plastic_kernel = self.add_weight(
            shape=(1, self.units, self.units),
            name='plastic_kernel',
            initializer=self.kernel_initializer,
            regularizer=self.kernel_regularizer,
            constraint=self.kernel_constraint,
        )
        self.plastic_lr = self.add_weight(
            shape=(1,),
            name='plastic_lr',
            initializer=self.bias_initializer,
            regularizer=self.bias_regularizer,
            constraint=self.bias_constraint,
        )
        super().build(input_shape)

    def call(self, inputs, states, training=None):
        h_tm1 = states[0]  # previous memory
        plastic_vals = K.reshape(states[1], (-1, self.units, self.units))

        if 0 < self.dropout < 1 and self._dropout_mask is None:
            self._dropout_mask = _generate_dropout_mask(
                K.ones_like(inputs),
                self.dropout,
                training=training,
                count=3,
            )
        if (0 < self.recurrent_dropout < 1 and
                self._recurrent_dropout_mask is None):
            self._recurrent_dropout_mask = _generate_dropout_mask(
                K.ones_like(h_tm1),
                self.recurrent_dropout,
                training=training,
                count=3,
            )

        # dropout matrices
        dp_mask = self._dropout_mask
        rec_dp_mask = self._recurrent_dropout_mask

        if 0. < self.dropout < 1.:
            inputs_z = inputs * dp_mask[0]
            inputs_r = inputs * dp_mask[1]
            inputs_h = inputs * dp_mask[2]
        else:
            inputs_z = inputs
            inputs_r = inputs
            inputs_h = inputs

        x_z = K.dot(inputs_z, self.kernel_z)
        x_r = K.dot(inputs_r, self.kernel_r)
        x_h = K.dot(inputs_h, self.kernel_h)
        if self.use_bias:
            x_z = K.bias_add(x_z, self.input_bias_z)
            x_r = K.bias_add(x_r, self.input_bias_r)
            x_h = K.bias_add(x_h, self.input_bias_h)

        if 0. < self.recurrent_dropout < 1.:
            h_tm1_z = h_tm1 * rec_dp_mask[0]
            h_tm1_r = h_tm1 * rec_dp_mask[1]
            h_tm1_h = h_tm1 * rec_dp_mask[2]
        else:
            h_tm1_z = h_tm1
            h_tm1_r = h_tm1
            h_tm1_h = h_tm1

        recurrent_z = K.dot(h_tm1_z, self.recurrent_kernel_z)
        recurrent_r = K.dot(h_tm1_r, self.recurrent_kernel_r)
        if self.reset_after and self.use_bias:
            recurrent_z = K.bias_add(recurrent_z, self.recurrent_bias_z)
            recurrent_r = K.bias_add(recurrent_r, self.recurrent_bias_r)

        z = self.recurrent_activation(x_z + recurrent_z)
        r = self.recurrent_activation(x_r + recurrent_r)

        # reset gate applied after/before matrix multiplication
        if self.reset_after:
            recurrent_h = K.dot(h_tm1_h, self.recurrent_kernel_h)
            if self.use_bias:
                recurrent_h = K.bias_add(recurrent_h, self.recurrent_bias_h)
            recurrent_h = r * recurrent_h
        else:
            recurrent_h = K.dot(r * h_tm1_h, self.recurrent_kernel_h)

        # Apply plasticity
        x_h = x_h + K.batch_dot(h_tm1, self.plastic_kernel * plastic_vals)

        hh = self.activation(x_h + recurrent_h)

        # previous and candidate state mixed by update gate
        h = z * h_tm1 + (1 - z) * hh

        plastic_vals = self.plastic_update(self.plastic_lr, h_tm1, h, plastic_vals)

        if 0 < self.dropout + self.recurrent_dropout:
            if training is None:
                h._uses_learning_phase = True

        return h, [h, K.batch_flatten(plastic_vals)]

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        return dict(**base_config, **config)


class PlasticGRU(GRU):
    """
    Augments the Keras GRU with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            activation='tanh',
            bias_constraint=None,
            bias_initializer='zeros',
            bias_regularizer=None,
            dropout=0.,
            go_backwards=False,
            kernel_constraint=None,
            kernel_initializer='glorot_uniform',
            kernel_regularizer=None,
            plasticity_rule='oja',
            recurrent_activation='hard_sigmoid',
            recurrent_constraint=None,
            recurrent_dropout=0.,
            recurrent_initializer='orthogonal',
            recurrent_regularizer=None,
            reset_after=False,
            return_sequences=False,
            return_state=False,
            stateful=False,
            unroll=False,
            use_bias=True,
            **kwargs,
    ):
        super().__init__(
            units,
            dropout=dropout,
            go_backwards=go_backwards,
            return_sequences=return_sequences,
            return_state=return_state,
            stateful=stateful,
            unroll=unroll,
            **kwargs,
        )

        self.plasticity_rule = plasticity_rule
        self.cell = PlasticGRUCell(
            units,
            activation=activation,
            bias_constraint=bias_constraint,
            bias_initializer=bias_initializer,
            bias_regularizer=bias_regularizer,
            dropout=dropout,
            kernel_constraint=kernel_constraint,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            plasticity_rule=plasticity_rule,
            recurrent_activation=recurrent_activation,
            recurrent_constraint=recurrent_constraint,
            recurrent_dropout=recurrent_dropout,
            recurrent_initializer=recurrent_initializer,
            recurrent_regularizer=recurrent_regularizer,
            reset_after=reset_after,
            use_bias=use_bias,
        )

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        del base_config['cell']
        return dict(**base_config, **config)


class PlasticLSTMCell(LSTMCell):
    """
    Augments the Keras LSTMCell with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            plasticity_rule='oja',
            **kwargs,
    ):
        super().__init__(units, **kwargs)
        self.state_size = (self.units, self.units, self.units ** 2)

        self.plastic_kernel = None
        self.plastic_lr = None
        self.plasticity_rule = plasticity_rule
        self.plastic_update = oja if plasticity_rule == 'oja' else hebb
        self.plastic_val = None

    def build(self, input_shape):
        self.plastic_kernel = self.add_weight(
            shape=(1, self.units, self.units),
            name='plastic_kernel',
            initializer=self.kernel_initializer,
            regularizer=self.kernel_regularizer,
            constraint=self.kernel_constraint,
        )
        self.plastic_lr = self.add_weight(
            shape=(1,),
            name='plastic_lr',
            initializer=self.bias_initializer,
            regularizer=self.bias_regularizer,
            constraint=self.bias_constraint,
        )
        super().build(input_shape)

    def call(self, inputs, states, training=None):
        if 0 < self.dropout < 1 and self._dropout_mask is None:
            self._dropout_mask = _generate_dropout_mask(
                K.ones_like(inputs),
                self.dropout,
                training=training,
                count=4,
            )
        if (0 < self.recurrent_dropout < 1 and
                self._recurrent_dropout_mask is None):
            self._recurrent_dropout_mask = _generate_dropout_mask(
                K.ones_like(states[0]),
                self.recurrent_dropout,
                training=training,
                count=4,
            )

        # dropout matrices
        dp_mask = self._dropout_mask
        rec_dp_mask = self._recurrent_dropout_mask

        h_tm1 = states[0]  # previous memory state
        c_tm1 = states[1]  # previous carry state
        plastic_vals = K.reshape(states[2], (-1, self.units, self.units))

        if 0 < self.dropout < 1.:
            inputs_i = inputs * dp_mask[0]
            inputs_f = inputs * dp_mask[1]
            inputs_c = inputs * dp_mask[2]
            inputs_o = inputs * dp_mask[3]
        else:
            inputs_i = inputs
            inputs_f = inputs
            inputs_c = inputs
            inputs_o = inputs
        x_i = K.dot(inputs_i, self.kernel_i)
        x_f = K.dot(inputs_f, self.kernel_f)
        x_c = K.dot(inputs_c, self.kernel_c)
        x_o = K.dot(inputs_o, self.kernel_o)
        if self.use_bias:
            x_i = K.bias_add(x_i, self.bias_i)
            x_f = K.bias_add(x_f, self.bias_f)
            x_c = K.bias_add(x_c, self.bias_c)
            x_o = K.bias_add(x_o, self.bias_o)

        if 0 < self.recurrent_dropout < 1.:
            h_tm1_i = h_tm1 * rec_dp_mask[0]
            h_tm1_f = h_tm1 * rec_dp_mask[1]
            h_tm1_c = h_tm1 * rec_dp_mask[2]
            h_tm1_o = h_tm1 * rec_dp_mask[3]
        else:
            h_tm1_i = h_tm1
            h_tm1_f = h_tm1
            h_tm1_c = h_tm1
            h_tm1_o = h_tm1
        i = self.recurrent_activation(x_i + K.dot(h_tm1_i, self.recurrent_kernel_i))
        f = self.recurrent_activation(x_f + K.dot(h_tm1_f, self.recurrent_kernel_f))
        c = f * c_tm1 + i * self.activation(x_c + K.dot(h_tm1_c, self.recurrent_kernel_c))
        o = self.recurrent_activation(x_o + K.dot(h_tm1_o, self.recurrent_kernel_o))

        # Apply plasticity
        c = c + K.batch_dot(c_tm1, self.plastic_kernel * plastic_vals)

        h = o * self.activation(c)

        plastic_vals = self.plastic_update(self.plastic_lr, h_tm1, h, plastic_vals)

        if 0 < self.dropout + self.recurrent_dropout:
            if training is None:
                h._uses_learning_phase = True
        return h, [h, c, K.batch_flatten(plastic_vals)]

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        return dict(**base_config, **config)


class PlasticLSTM(LSTM):
    """
    Augments the Keras LSTM with a differentiable plasticity mechanism.

    Reference:
        https://arxiv.org/abs/1804.02464
    """
    def __init__(
            self,
            units,
            activation='tanh',
            activity_regularizer=None,
            bias_constraint=None,
            bias_initializer='zeros',
            bias_regularizer=None,
            dropout=0.,
            go_backwards=False,
            kernel_constraint=None,
            kernel_initializer='glorot_uniform',
            kernel_regularizer=None,
            plasticity_rule='oja',
            recurrent_activation='hard_sigmoid',
            recurrent_constraint=None,
            recurrent_dropout=0.,
            recurrent_initializer='orthogonal',
            recurrent_regularizer=None,
            return_sequences=False,
            return_state=False,
            stateful=False,
            unit_forget_bias=True,
            unroll=False,
            use_bias=True,
            **kwargs,
    ):
        super().__init__(
            units,
            go_backwards=go_backwards,
            return_sequences=return_sequences,
            return_state=return_state,
            stateful=stateful,
            unroll=unroll,
            **kwargs,
        )

        self.plasticity_rule = plasticity_rule
        self.cell = PlasticLSTMCell(
            units,
            activation=activation,
            bias_constraint=bias_constraint,
            bias_initializer=bias_initializer,
            bias_regularizer=bias_regularizer,
            dropout=dropout,
            kernel_constraint=kernel_constraint,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            plasticity_rule=plasticity_rule,
            recurrent_activation=recurrent_activation,
            recurrent_constraint=recurrent_constraint,
            recurrent_dropout=recurrent_dropout,
            recurrent_initializer=recurrent_initializer,
            recurrent_regularizer=recurrent_regularizer,
            unit_forget_bias=unit_forget_bias,
            use_bias=use_bias,
        )

    def get_config(self):
        config = {'plasticity_rule': self.plasticity_rule}
        base_config = super().get_config()
        del base_config['cell']
        return dict(**base_config, **config)
