"""!
Implements Age Fitness Pareto Optimization (AFPO) and supporting functions.

Borrowed some code from the DEAP.

Author: Colin M. Van Oort
"""


import argparse
import logging
import sys
from copy import deepcopy
from pathlib import Path

import gym
import gym_vail
import keras
import numpy as np
import tensorflow as tf
from deap import algorithms
from deap import base
from deap import tools
from keras.layers import Input, SimpleRNN, GRU, LSTM, Dense, BatchNormalization, Activation
from keras.models import Model
from keras.utils import to_categorical

import plastic_layers


def get_parser():
    parser = argparse.ArgumentParser(
        description="Trains a simple network on an OpenAI Gym task using AFPO.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-u',
        '--updates',
        type=int,
        default=50,
        help='Number of training updates applied to the network.',
    )
    parser.add_argument(
        '-p',
        '--population_count',
        type=int,
        default=50,
        help='Number of individuals used in the evolutionary population.',
    )
    parser.add_argument(
        '-s',
        '--max_steps',
        type=int,
        default=250,
        help='Maximum number of time steps in a single rollout.',
    )
    parser.add_argument(
        '-m',
        '--model_type',
        type=str,
        default='plastic_rnn',
        choices=['rnn', 'gru', 'lstm', 'plastic_rnn', 'plastic_gru', 'plastic_lstm'],
        help='Primary layer used to construct the agent network.',
    )
    parser.add_argument(
        '-n',
        '--neurons',
        type=int,
        default=32,
        help='Number neurons used in hidden layers.',
    )
    parser.add_argument(
        '-e',
        '--env_name',
        type=str,
        default='Maze-v0',
        help='OpenAI Gym environment to use as the learning task.',
    )
    parser.add_argument(
        '-c',
        '--env_count',
        type=int,
        default=16,
        help='Number of environments to create using gym.vector.',
    )
    parser.add_argument(
        '-r',
        '--render',
        type=bool,
        default=False,
        help='Determines whether an animation is displayed for the agent rollouts.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )

    return parser


def main(
        env_count=1,
        env_name='CartPole-v1',
        eval_eps=10,
        max_steps=500,
        model_type='plastic_rnn',
        neurons=32,
        population_count=100,
        render=False,
        updates=10,
        verbose=0,
        **kwargs,
):
    # Prevent TensorFlow from allocating all available GPU memory
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    keras.backend.set_session(tf.Session(config=config))

    initargs = [env_name, env_count, max_steps, model_type, neurons]
    agent = AFPOAgent(*initargs)

    toolbox = KerasAFPOToolbox(
        model_shapes=[w.shape for w in agent.model.get_weights()],
        eval_fn=agent.evaluate,
        tournament_size=7,
        mate_indpb=0.2,
        mut_mu=0.,
        mut_sigma=0.3,
        mut_indpb=0.005,
    )

    pop = toolbox.population(count=population_count)
    hof = tools.HallOfFame(25, similar=np.array_equal)

    stats = tools.Statistics(lambda ind: ind.fitness.values[0])
    stats.register("min", np.min)
    stats.register("median", np.median)
    stats.register("max", np.max)
    stats.register("mean", np.mean)

    pop, logbook = afpo(
        pop,
        tool_box=toolbox,
        crossover_rate=0.25,
        mutation_rate=0.75,
        generations=updates,
        stats=stats,
        hall_of_fame=hof,
        verbose=verbose,
    )

    mean_fit, max_fit = logbook.select('mean', 'max')

    scores = []
    for ind in hof:
        score, age = agent.evaluate(ind, episodes=eval_eps)
        scores.append(score)
    with open(f'../out/{model_type}/mean_reward_paths.csv', 'a') as f:
        print(str(mean_fit)[1:-1], file=f)
    with open(f'../out/{model_type}/max_reward_paths.csv', 'a') as f:
        print(str(max_fit)[1:-1], file=f)
    with open(f'../out/{model_type}/mean_eval_rewards.csv', 'a') as f:
        print(max(scores), file=f)

    if verbose:
        logging.info(f'Population Max Evaluation Score: {max(scores)}')


class AFPOAgent:
    def __init__(self, env, env_count, max_env_steps=500, net_type='plastic_rnn', neurons=20):
        self.env = gym.vector.make(env, env_count, asynchronous=False) if isinstance(env, str) else env
        self.env_count = env_count
        self.max_env_steps = max_env_steps

        self.net_type = net_type
        self.neurons = neurons

        self.num_actions = self.env.action_space[0].n
        self.obs_shape = (1, self.env.observation_space.shape[-1] + self.num_actions + 1,)

        # CPU execution is currently faster than GPU execution (~13 mins vs ~35 mins)
        # This is most likely due to the high overhead of sending data to the GPU
        # combined with the large amount of CPU-GPU communication involved in a policy rollout.
        self.model = self.build_model(device='cpu:0')

    def build_model(self, device='cpu:0'):
        with tf.device(device):
            obs = Input(batch_shape=(self.env_count, *self.obs_shape))

            if self.net_type == 'plastic_rnn':
                pred = plastic_layers.PlasticRNN(self.neurons, stateful=True)(obs)
            elif self.net_type == 'plastic_gru':
                pred = plastic_layers.PlasticGRU(self.neurons, stateful=True)(obs)
            elif self.net_type == 'plastic_lstm':
                pred = plastic_layers.PlasticLSTM(self.neurons, stateful=True)(obs)
            elif self.net_type == 'rnn':
                pred = SimpleRNN(self.neurons, stateful=True)(obs)
            elif self.net_type == 'gru':
                pred = GRU(self.neurons, stateful=True)(obs)
            elif self.net_type == 'lstm':
                pred = LSTM(self.neurons, stateful=True)(obs)
            else:
                raise ValueError(f'Unrecognized model_type: {self.net_type}!')

            pred = Dense(self.env.action_space[0].n)(pred)
            pred = BatchNormalization()(pred)
            pred_actions = Activation('softmax')(pred)

            model = Model(inputs=obs, outputs=pred_actions)
            model.compile(optimizer='adam', loss='mse')
        return model

    def get_action(self, obs):
        obs = np.expand_dims(obs, axis=1)
        action_probs = self.model.predict(obs, batch_size=self.env_count)
        return np.argmax(action_probs, axis=-1)

    def rollout(self, render=False):
        states = self.env.reset()
        episode_rewards = np.zeros((self.env.num_envs, 1))
        action_rewards = np.zeros((self.env.num_envs, 1))
        reward_mask = np.ones((self.env.num_envs, 1))
        actions = np.zeros((self.env.num_envs, self.num_actions))

        for _ in range(self.max_env_steps):
            actions = self.get_action(np.hstack([states, actions, action_rewards]))
            next_states, action_rewards, dones, _ = self.env.step(actions)
            reward_mask *= np.logical_not(dones)[..., np.newaxis]
            action_rewards = action_rewards[..., np.newaxis] * reward_mask
            actions = to_categorical(actions, num_classes=self.num_actions)
            episode_rewards += action_rewards

            if render:
                self.env.render()
            if np.all(np.logical_not(reward_mask)):
                break
            else:
                states = next_states
        self.model.reset_states()
        return np.median(episode_rewards)

    def evaluate(self, individual=None, episodes=1, render=False):
        if individual:
            self.model.set_weights(individual.weights)

        rewards = [self.rollout(render=render) for _ in range(episodes)]

        if individual:
            return np.median(rewards), individual.age
        else:
            return np.median(rewards)


def afpo(
        population,
        tool_box,
        crossover_rate,
        mutation_rate,
        generations,
        births=0,
        stats=None,
        hall_of_fame=None,
        verbose=True,
        map_fn=map,
):
    """
    Implements Age Fitness Pareto Optimization.

    Args:
        population: (list)
        tool_box: (deap.base.Toolbox) Holds evolutionary operators.
        crossover_rate: (float)
        mutation_rate: (float)
        generations: (int)
        births: (int)
        stats: (deap.tools.Statistics) Logs evolution dynamics.
        hall_of_fame: (deap.tools.HallOfFame) Holds best individuals.
        verbose: (bool) Terminal output control.
        map_fn: (Callable)

    Returns:
        (list) Evolved population, evolution logbook.
    """
    assert births >= 0
    pop_count = len(population)
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    population += [tool_box.individual() for _ in range(births)]
    fitnesses = map_fn(tool_box.evaluate, population)
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit

    if hall_of_fame is not None:
        hall_of_fame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(population), **record)

    if verbose:
        print(logbook.stream)

    # The generational loop
    for gen in range(1, generations + 1):
        offspring = tool_box.select(population, pop_count - births)
        offspring = algorithms.varAnd(offspring, tool_box, crossover_rate, mutation_rate)
        offspring += [tool_box.individual() for _ in range(births)]

        invalids = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map_fn(tool_box.evaluate, invalids)
        for ind, fit in zip(invalids, fitnesses):
            ind.fitness.values = fit

        if hall_of_fame is not None:
            hall_of_fame.update(offspring)

        population[:] = offspring

        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalids), **record)

        if verbose:
            print(logbook.stream)

        for individual in population:
            individual.age += 1

    return population, logbook


class KerasAFPOToolbox:
    def __init__(
            self,
            model_shapes,
            eval_fn,
            tournament_size,
            mate_indpb,
            mut_mu,
            mut_sigma,
            mut_indpb,
            map_fn=map,
    ):
        self.model_shapes = model_shapes
        self.eval_fn = eval_fn
        self.tournament_size = tournament_size
        self.mate_indpb = mate_indpb
        self.mut_mu = mut_mu
        self.mut_sigma = mut_sigma
        self.mut_indpb = mut_indpb
        self.map_fn = map_fn

    @staticmethod
    def clone(individual):
        return deepcopy(individual)

    def individual(self):
        return Individual(
            weights=[2 * np.random.random_sample(shape) - 1. for shape in self.model_shapes],
            fitness=AFPOFitness(),
        )

    def population(self, count):
        return [self.individual() for _ in range(count)]

    def select(self, population, count):
        """
        Args:
            population: (list) Potential tournament contestants.
            count: (int)

        Return: (list)
            Winners of each tournament.
        """
        selected = []

        for i in range(count):
            contestants = tools.selRandom(population, self.tournament_size)
            selected.append(tools.sortLogNondominated(contestants, 1, first_front_only=True)[0])
        return selected

    def mate(self, ind1, ind2):
        """!
        Randomly swaps weights between two Keras models.

        The mask array used to swap weights should have a boolean dtype to ensure
        that the copied components are not views of the original array.
        Additionally, this may have incorrect behavior if the individuals share
        any underlying data.

        Reference:
            http://numpy-discussion.10968.n7.nabble.com/swap-elements-in-two-arrays-td17916.html

        Args:
            ind1: (List[np.ndarray])
            ind2: (List[np.ndarray])

        Return: (tuple)
            Two individuals resulting from crossover.
        """
        swap_masks = [np.random.random(size=x.shape) < self.mate_indpb for x in ind1]
        for swap_mask, w1, w2 in zip(swap_masks, ind1, ind2):
            w1[swap_mask], w2[swap_mask] = w2[swap_mask], w1[swap_mask]

        ind1.age = max(ind1.age, ind2.age)
        ind2.age = max(ind1.age, ind2.age)

        return ind1, ind2

    def mutate(self, ind):
        """
        Args:
            ind: (List[np.ndarray])

        Returns: (List[np.ndarray])
            Mutated model weights.
        """
        muts = [np.random.normal(loc=self.mut_mu, scale=self.mut_sigma, size=x.shape) for x in ind]
        masks = [np.random.random(size=x.shape) < self.mut_indpb for x in ind]
        new_weights = [w + mut * mask for w, mut, mask in zip(ind.weights, muts, masks)]
        ind.weights = new_weights
        return ind,

    def map(self, func, iterable):
        return self.map_fn(func, iterable)

    def evaluate(self, individual, **kwargs):
        return self.eval_fn(individual, **kwargs)


class Individual:
    def __init__(self, weights, fitness=None):
        self.age = 0
        self.fitness = fitness or AFPOFitness()
        self.weights = weights

    def __getitem__(self, item):
        return self.weights[item]

    def __setitem__(self, key, value):
        self.weights[key] = value

    def __len__(self):
        return len(self.weights)


class AFPOFitness(base.Fitness):
    weights = (1.0, -1.0)


if __name__ == '__main__':
    args = vars(get_parser().parse_args())

    Path(f'../out/{args["model_type"]}').mkdir(parents=True, exist_ok=True)
    logging.basicConfig(
        filename=f'../out/{args["model_type"]}/{args["model_type"]}.log',
        level=logging.INFO,
        format='%(message)s',
    )
    if args['verbose']:
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info('Command line call:\n\tpython {}'.format(' '.join(sys.argv)))

    main(**args)
